/*
 * Data generator
 * Usage: ./dg dim
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void rand_mat(double *mat, int dim)
{
  int i,j; //iterators
 
  for(i=0; i<dim; i++) //iterate through the rows
    for(j=0; j<dim; j++) //iterate through the columns
      *(mat+(j+i*dim)) = (10.0*rand())/RAND_MAX;
} //end rand_mat()

//fprint_mat() prints square matrix 'mat' of dimension 'dim' to the file 'flnm'
void fprint_mat(const char * flnm, double *mat, int dim)
{
  int i,j;
  FILE *fl;
  fl = fopen(flnm,"w"); //open the file
	fprintf(fl,"%d %d\n", dim, dim);
  for(i=0; i<dim; i++){
    for(j=0; j<dim; j++)
      fprintf(fl,"%1.0f ",*( mat+(j+i*dim) ));
    fprintf(fl,"\n");
  }
  fclose(fl); //close the file
} //end fprint_mat()


//main() does the work
int main(int argc, char *argv[] ) {

	if (argc < 2){
		printf("Usage: ./dg dim");
		exit(-1);
	}

	int dim = atoi(argv[1]);
  double *mat1=NULL, *mat2=NULL;

  //allocate memory for the two matrices
  mat1 = (double *)malloc(dim*dim*sizeof(double));
  if(mat1==NULL) printf("\nUnable to allocate memory for matrix 1.\n");
  mat2 = (double *)malloc(dim*dim*sizeof(double));
  if(mat2==NULL) printf("\nUnable to allocate memory for matrix 2.\n");

  printf("\nGenerating two random matrices...");
  srand( (unsigned int)time(NULL) );
  printf("mat1 (%d x %d)...",dim,dim);
  rand_mat(mat1, dim);
  printf("Done. mat2 (%d x %d)...",dim,dim);
  rand_mat(mat2, dim);
  printf("Done.\n");
	
  //write the two matrices to their respective files
	printf("Write two matrices to the disk.\n");
  fprint_mat("./data/mdata1.inp",mat1,dim);
  fprint_mat("./data/mdata2.inp",mat2,dim);
  printf("Done.\n");
}
