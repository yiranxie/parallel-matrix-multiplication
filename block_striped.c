/*
 ******************************************************************** 
 Block striped method

 Objective           : Matrix Matrix multiplication
 
 Input               : Read files (mdata1.inp) for first input matrix 
 and (mdata2.inp) for second input matrix 
 
 Output              : Result of matrix matrix multiplication on Processor 0.
 
 ********************************************************************
 */



#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include "mpi.h"

int main (int argc, char *argv[])
{

	int istage,irow,icol,jrow,jcol,index,Proc_Id,Root =0;
	int A_Bloc_MatrixSize, B_Bloc_MatrixSize;
	int NoofRows_A, NoofCols_A, NoofRows_B, NoofCols_B;
	int NoofRows_BlocA, NoofCols_BlocA, NoofRows_BlocB, NoofCols_BlocB;
	int Matrix_Size[4];
	int source, destination, send_tag, recv_tag;

	float *Matrix_A, *Matrix_B, **Matrix_C;
	float *A_Bloc_Matrix, *B_Bloc_Matrix, *C_Bloc_Matrix, *Temp_BufferA;
	float * MatA_array, * MatB_array, * MatC_array;
	FILE *fp;
	int	MatrixA_FileStatus = 1, MatrixB_FileStatus = 1;
	int procNum;
	int myRank;

	int debug = 0;
	double startTime, endTime;
	double loadDataTime = 0.0, prepareDataTime = 0.0, memTime = 0.0, scatterTime = 0.0, computationTime = 0.0,
				 sendAndRecieveTime = 0.0, storeTime = 0.0;
	double temp1, temp2;

	startTime = MPI_Wtime();

	MPI_Status status; 

	/* Initialising */
	MPI_Init (&argc, &argv);

	MPI_Comm_size(MPI_COMM_WORLD, &procNum);
	MPI_Comm_rank(MPI_COMM_WORLD, &myRank);

	/* Reading Input */
	if (myRank == Root){
		temp1 = MPI_Wtime();

		if ((fp = fopen ("./data/mdata1.inp", "r")) == NULL){
			MatrixA_FileStatus = 0;
		}

		if(MatrixA_FileStatus != 0) {
			fscanf(fp, "%d %d\n", &NoofRows_A, &NoofCols_A);
			Matrix_Size[0] = NoofRows_A;
			Matrix_Size[1] = NoofCols_A;

			Matrix_A = (float *) malloc (NoofRows_A * NoofCols_A * sizeof(float));
			for (irow = 0; irow < NoofRows_A; irow++){
				for (icol = 0; icol < NoofCols_A; icol++)
					fscanf(fp, "%f", Matrix_A + irow * NoofCols_A + icol);
			}
			fclose(fp);
		}

		if((fp = fopen ("./data/mdata2.inp", "r")) == NULL){
			MatrixB_FileStatus = 0;
		}

		if(MatrixB_FileStatus != 0) {
			fscanf(fp, "%d %d\n", &NoofRows_B, &NoofCols_B);
			Matrix_Size[2] = NoofRows_B;
			Matrix_Size[3] = NoofCols_B;

			Matrix_B = (float *) malloc (NoofRows_B * NoofCols_B * sizeof(float));
			for(irow = 0; irow < NoofRows_B; irow++){
				for(icol = 0; icol < NoofCols_B; icol++)
					fscanf(fp, "%f", Matrix_B + irow * NoofCols_B  + icol);
			}
			fclose(fp);
		}

		temp2 = MPI_Wtime();
		loadDataTime = temp2 - temp1;	//Accumulate the data load time
	} /* MyRank == Root */

	temp1 = MPI_Wtime();
	/*  Send Matrix Size to all processors  */
	MPI_Barrier(MPI_COMM_WORLD);

	MPI_Bcast (&MatrixA_FileStatus, 1, MPI_INT, 0, MPI_COMM_WORLD);
	if(MatrixA_FileStatus == 0) {
		if(myRank == 0) printf("Can't open input file for Matrix A ..");
		MPI_Finalize();
		exit(-1);
	}
	MPI_Bcast (&MatrixB_FileStatus, 1, MPI_INT, 0, MPI_COMM_WORLD);
	if(MatrixB_FileStatus == 0) {
		if(myRank == 0) printf("Can't open input file for Matrix B ..");
		MPI_Finalize();
		exit(-1);
	}

	MPI_Bcast (Matrix_Size, 4, MPI_INT, 0, MPI_COMM_WORLD);

	NoofRows_A = Matrix_Size[0];
	NoofCols_A = Matrix_Size[1];
	NoofRows_B = Matrix_Size[2];
	NoofCols_B = Matrix_Size[3];

	if(NoofCols_A != NoofRows_B){
		MPI_Finalize();
		if(myRank == Root){
			printf("Matrices Dimensions incompatible for Multiplication");
		}
		exit(-1);
	}

	if( NoofRows_A % procNum != 0 || NoofCols_A % procNum != 0 ||
			NoofRows_B % procNum != 0 || NoofCols_B % procNum != 0){

		MPI_Finalize();
		if(myRank == Root){
			printf("Matrices can't be divided among processors equally");
		}
		exit(-1);
	}

	temp2 = MPI_Wtime();
	sendAndRecieveTime += temp2 - temp1; //Accumulate the send and receive time

	NoofRows_BlocA = NoofRows_A / procNum;
	NoofCols_BlocA = NoofCols_A;

	NoofRows_BlocB = NoofRows_B;
	NoofCols_BlocB = NoofCols_B / procNum;

	A_Bloc_MatrixSize = NoofRows_BlocA * NoofCols_BlocA;
	B_Bloc_MatrixSize = NoofRows_BlocB * NoofCols_BlocB;

	temp1 = MPI_Wtime();
	/* Memory allocating for Bloc Matrices */
	A_Bloc_Matrix = (float *) malloc (A_Bloc_MatrixSize * sizeof(float));
	B_Bloc_Matrix = (float *) malloc (B_Bloc_MatrixSize * sizeof(float));
	temp2 = MPI_Wtime();
	memTime += temp2 - temp1;

	/*Rearrange the input matrices in one dim arrays by approriate order*/
	if (myRank == Root) {
		temp1 = MPI_Wtime();

		/* memory for arrangmeent of the data in one dim. arrays before MPI_SCATTER */
		MatA_array =(float *)malloc(sizeof(float) * NoofRows_A * NoofCols_A);
		MatB_array =(float *)malloc(sizeof(float) * NoofRows_B * NoofCols_B);

		/* Rearranging Matrix A*/
		for (irow = 0; irow < NoofRows_A; irow++){
			for (icol = 0; icol < NoofCols_A; icol++){
				MatA_array[irow * NoofCols_A + icol] = Matrix_A[irow * NoofCols_A + icol];
			}
		}
		
		/* Rearranging Matrix B*/
		for (irow = 0; irow < NoofRows_B; irow++){
			for (icol = 0; icol < NoofCols_B; icol++){
				MatB_array[icol * NoofRows_B + irow] = Matrix_B[irow * NoofCols_B + icol];
			}
		}
		/* Release the memory */
		if (debug == 0){
			/* free Matrix_A and Matrix_B */
			free(Matrix_A);
			free(Matrix_B);
		}

		temp2 = MPI_Wtime();
		prepareDataTime += temp2 - temp1;	//Accumlate the prepare data time
	} /* if loop ends here */

	temp1 = MPI_Wtime();

	MPI_Barrier(MPI_COMM_WORLD);

	/* Scatter the Data  to all processes by MPI_SCATTER */
	MPI_Scatter (MatA_array, A_Bloc_MatrixSize, MPI_FLOAT, A_Bloc_Matrix ,
			A_Bloc_MatrixSize , MPI_FLOAT, 0, MPI_COMM_WORLD);

	MPI_Scatter (MatB_array, B_Bloc_MatrixSize, MPI_FLOAT, B_Bloc_Matrix,
			B_Bloc_MatrixSize, MPI_FLOAT, 0, MPI_COMM_WORLD);

	if (myRank == Root) {
		free(MatA_array);
		free(MatB_array);
	}

	temp2 = MPI_Wtime();
	scatterTime = temp2 - temp1;	//Accumulate the scatter time

	temp1 = MPI_Wtime();
	/* Allocate Memory for Bloc C Array */
	C_Bloc_Matrix = (float *) malloc (NoofRows_BlocA * NoofCols_B * sizeof(float));
	for(index=0; index<NoofRows_BlocA*NoofCols_B; index++)
		C_Bloc_Matrix[index] = 0;

	temp2 = MPI_Wtime();
	memTime += temp2 - temp1;

	/* The main loop */

	send_tag = 0;
	recv_tag = 0;
	for(istage=0; istage<procNum; istage++){

		temp1 = MPI_Wtime();

		for(irow=0; irow<NoofRows_BlocA; irow++){
			index = (myRank - istage + procNum) % procNum * NoofCols_BlocB + irow * NoofCols_B;
			for(icol=0; icol<NoofCols_BlocB; icol++){
				for(jcol=0; jcol<NoofCols_BlocA; jcol++){
					C_Bloc_Matrix[index] += A_Bloc_Matrix[irow*NoofCols_BlocA + jcol] *
						B_Bloc_Matrix[jcol + icol*NoofCols_BlocA];
				}
//				printf("proc:%d, %d, %f\n", myRank, index, C_Bloc_Matrix[index]);
				index++;
			}
		}

		temp2 = MPI_Wtime();
		computationTime += temp2 - temp1;

		if (istage != procNum - 1){
			temp1 = MPI_Wtime();

			/* Move Stripe of Matrix B by one position right */
			source   = (myRank - 1 + procNum) % procNum;
			destination = (myRank + 1) % procNum;
			MPI_Sendrecv_replace(B_Bloc_Matrix, B_Bloc_MatrixSize, MPI_FLOAT,
					destination, send_tag, source, recv_tag, MPI_COMM_WORLD, &status);

			temp2 = MPI_Wtime();
			sendAndRecieveTime += temp2 - temp1;
		}
	}

	/* Memory for output global matrix in the form of array  */
	if(myRank == Root){ 
		temp1 = MPI_Wtime();
		MatC_array = (float *) malloc (sizeof(float) * NoofRows_A * NoofCols_B);
		temp2 = MPI_Wtime();
	  prepareDataTime += temp2 - temp1;
	}

	temp1 = MPI_Wtime();
	MPI_Barrier(MPI_COMM_WORLD);

	/* Gather output block matrices at processor 0 */
	MPI_Gather (C_Bloc_Matrix, NoofRows_BlocA * NoofCols_B, MPI_FLOAT,
			MatC_array, NoofRows_BlocA * NoofCols_B, MPI_FLOAT, Root, MPI_COMM_WORLD);
	temp2 = MPI_Wtime();
	sendAndRecieveTime += temp2 - temp1;

	/* Free A_Bloc_Matrix, B_Bloc_Matrix, C_Bloc_Matrix, Temp_BufferA */
	temp1 = MPI_Wtime();
	free(A_Bloc_Matrix);
	free(B_Bloc_Matrix);
	free(C_Bloc_Matrix);
	temp2 = MPI_Wtime();
	memTime += temp2 - temp1;

	/* Memory for output global array for OutputMatrix_C after rearrangement */
	if(myRank == Root){
		temp1 = MPI_Wtime();
		Matrix_C = (float **) malloc (NoofRows_A * sizeof(float *));
		for(irow=0; irow<NoofRows_A ;irow++)
			Matrix_C[irow] = (float *) malloc(NoofCols_B * sizeof(float));
		temp2 = MPI_Wtime();
		prepareDataTime += temp2 - temp1;
	}

	/* Rearranging the output matrix in a array by approriate order  */
	if (myRank == Root) {
		temp1 = MPI_Wtime();
		/* Rearranging Matrix A*/
		for (irow = 0; irow < NoofRows_A; irow++){
			for (icol = 0; icol < NoofCols_B; icol++){
				Matrix_C[irow][icol] = MatC_array[irow * NoofCols_B + icol];
			}
		}
		free(MatC_array);		//release MatC_array
		temp2 = MPI_Wtime();
		prepareDataTime += temp2 - temp1;

		/* output the result */
		temp1 = MPI_Wtime();
		fp = fopen("block_result.txt", "w");
		for(irow = 0; irow < NoofRows_A; irow++){
			for(icol = 0; icol < NoofCols_B; icol++){
				fprintf(fp, "%7.1f ", Matrix_C[irow][icol]);
			}
			fprintf(fp, "\n");
		}
		fclose(fp);
		temp2 = MPI_Wtime();
		storeTime += temp2 - temp1;

		/* debug information */
		if (debug != 0){
			printf ("-----------MATRIX MULTIPLICATION RESULTS --------------\n");
			printf(" Processor %d, Matrix A : Dimension %d * %d : \n",
					myRank, NoofRows_A, NoofCols_A);
			for(irow = 0; irow < NoofRows_A; irow++) {
				for(icol = 0; icol < NoofCols_A; icol++)
					printf ("%1.4f ", Matrix_A[irow * NoofCols_A + icol]);
				printf ("\n");
			}
			printf("\n");

			printf("Processor %d, Matrix B : Dimension %d * %d : \n",
					myRank, NoofRows_B, NoofCols_B);
			for(irow = 0; irow < NoofRows_B; irow++){
				for(icol = 0; icol < NoofCols_B; icol++)
					printf("%1.4f ", Matrix_B[irow * NoofCols_B + icol]);
				printf("\n");
			}
			printf("\n");

			printf("Processor %d, Matrix C : Dimension %d * %d : \n",
					myRank, NoofRows_A, NoofCols_B);
			for(irow = 0; irow < NoofRows_A; irow++){
				for(icol = 0; icol < NoofCols_B; icol++)
					printf("%1.4f ",Matrix_C[irow][icol]);
				printf("\n");
			}

			/* free Matrix_A, Matrix_B and Matrix_C*/
			free(Matrix_A);
			free(Matrix_B);
		}

		for (irow = 0; irow < NoofRows_A; irow++){
			free(Matrix_C[irow]);
		}
		free(Matrix_C);
	}
	MPI_Finalize();

	endTime = MPI_Wtime();
	if (myRank == Root){
		printf("total,%f,sec\n", endTime-startTime);
		printf("load,%f,sec\n", loadDataTime);
		printf("prepare,%f,sec\n", prepareDataTime);
		printf("mem,%f,sec\n", memTime);
		printf("scatter,%f,sec\n", scatterTime);
		printf("computation,%f,sec\n", computationTime);
		printf("message,%f,sec\n", sendAndRecieveTime);
		printf("store,%f,sec\n", storeTime);
		//printf("Processor %d, Total time: %f sec.\n", myRank, endTime-startTime);
		//printf("\t==> Load data time: %f sec.\n", loadDataTime);
		//printf("\t==> Prepare data time: %f sec.\n", prepareDataTime);
		//printf("\t==> Memory Allocation/Deallocation data time: %f sec.\n", memTime);
		//printf("\t==> Scatter data time: %f sec.\n", scatterTime);
		//printf("\t==> Computation time: %f sec.\n", computationTime);
		//printf("\t==> Send and receive message time: %f sec.\n", sendAndRecieveTime);
		//printf("\t==> Store data time: %f sec.\n", storeTime);
	}

	return 0;
}
