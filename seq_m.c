#include<stdio.h>
#include<stdlib.h>
#include<sys/time.h>

//single processor solution 
void single_mul(float *A, float *B, float *AB, int dim)
{
	int i, j, k;
	for(i=0; i<dim; i++){	//iterate through the rows
		for(j=0; j<dim; j++){ //iterate through the columns
			for(k=0, *(AB+(j+i*dim))=0; k<dim; k++){
				*(AB+(j+i*dim)) += *(A+(k+i*dim)) * *(B+(j+k*dim));
			}
		}
	}
} // cmul

double loadData(const char * filename, int * pNoofRows, int * pNoofCols, float ** Matrix){

	struct timeval tv;
	long temp1 = 0, temp2 = 0;

	int irow,icol;
	FILE *fp;
	if ((fp = fopen (filename, "r")) != NULL){
		fscanf(fp, "%d %d\n", pNoofRows, pNoofCols);
		gettimeofday(&tv, NULL);
		long temp1 = 1000000 * tv.tv_sec + tv.tv_usec;
		*Matrix = (float *) malloc (*pNoofRows * *pNoofCols * sizeof(float));
		gettimeofday(&tv, NULL);
		long temp2 = 1000000 * tv.tv_sec + tv.tv_usec;
		for (irow = 0; irow < *pNoofRows; irow++){
			for (icol = 0; icol < *pNoofCols; icol++)
				fscanf(fp, "%f", (*Matrix) + (irow * *pNoofCols + icol));
		}
		fclose(fp);
	}

	return ((double) (temp2 - temp1)) / 1000000.0;
}

void storeData(const char * filename, int NoofRows, int NoofCols, float * Matrix){
	int irow,icol;
	FILE *fp;
	fp = fopen(filename, "w");
	for(irow = 0; irow < NoofRows; irow++){
		for(icol = 0; icol < NoofCols; icol++){
			fprintf(fp, "%7.1f ", *(Matrix + (irow * NoofCols) + icol));
		}
		fprintf(fp, "\n");
	}
	fclose(fp);
}

int main(int argc, char *argv[] ) {

	int	 MatrixA_FileStatus = 1, MatrixB_FileStatus = 1;
	int NoofRows_A, NoofCols_A, NoofRows_B, NoofCols_B;
	float *Matrix_A, *Matrix_B, *Matrix_C;
	double temp1, temp2;
	struct timeval tv;
	double loadTime, computeTime, saveTime, memTime;
	long startTime, endTime;

	gettimeofday(&tv, NULL);
	temp1 = 1000000 * tv.tv_sec + tv.tv_usec;
	startTime = temp1;

	/* Load data from disk */
	memTime = loadData("./data/mdata1.inp", &NoofRows_A, &NoofCols_A, &Matrix_A);
	memTime += loadData("./data/mdata2.inp", &NoofRows_B, &NoofCols_B, &Matrix_B);

	gettimeofday(&tv, NULL);
	temp2 = 1000000 * tv.tv_sec + tv.tv_usec;
	loadTime = ((double)(temp2 - temp1)) / 1000000.0 - memTime;

	Matrix_C = (float *) malloc(NoofRows_A * NoofCols_B * sizeof(float));

	gettimeofday(&tv, NULL);
	temp1 = 1000000 * tv.tv_sec + tv.tv_usec;

	single_mul(Matrix_A, Matrix_B, Matrix_C, NoofRows_A);

	gettimeofday(&tv, NULL);
	temp2 = 1000000 * tv.tv_sec + tv.tv_usec;
	computeTime = ((double)(temp2 - temp1)) / 1000000.0;

	gettimeofday(&tv, NULL);
	temp1 = 1000000 * tv.tv_sec + tv.tv_usec;

	free(Matrix_A);
	free(Matrix_B);

	gettimeofday(&tv, NULL);
	temp2 = 1000000 * tv.tv_sec + tv.tv_usec;
	memTime += ((double)(temp2 - temp1)) / 1000000.0;

	/* output the result */
	gettimeofday(&tv, NULL);
	temp1 = 1000000 * tv.tv_sec + tv.tv_usec;

	/* Load data from disk */
	storeData("seq_result.txt", NoofRows_A, NoofCols_A, Matrix_C);
	free(Matrix_C);

	gettimeofday(&tv, NULL);
	temp2 = 1000000 * tv.tv_sec + tv.tv_usec;
	saveTime = ((double)(temp2 - temp1)) / 1000000.0;
	endTime = temp2;

	printf("total,%f,sec\n", (endTime-startTime)/1000000.0);
	printf("load,%f,sec\n", loadTime);
	printf("mem,%f,sec\n", memTime);
	printf("computation,%f,sec\n", computeTime);
	printf("store,%f,sec\n", saveTime);
	return 0;
}
