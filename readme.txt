Three parallel matrix multiplication methods are implemented using OpenMPI library.
 
(1) Block-striped decomposition method
(2) Fox's method
(3) Cannon's method 